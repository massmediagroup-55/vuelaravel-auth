<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterFormRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Mail\AccoutnActivation;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App;

class AuthController extends Controller
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function register(RegisterFormRequest $request)
    {
        $newUser = $this->user::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'passwd' => 'not_activated',
            'passwd2' => md5($request['name'] . $request['password']),
            'Promt' => $request['Promt'],
            'answer' => $request['answer'],
            'idnumber' => request()->ip(),
            'creatime' => new \DateTime('now'),
        ]);
        event(new Registered($newUser));
        return response([
            'status' => 'success',
            'data' => $this->user
        ], 200);
    }

    public function login(LoginRequest $request)
    {
        $user = User::query()->where('name', $request->get('name'))
            ->where('passwd', md5($request->get('name') . $request->get('password')))
            ->first();

        if (!$user) {
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Invalid Credentials.'
            ], 400);
        }
        return response(['status' => 'success'])->header('Authorization', auth('api')->login($user));
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = User::query()->where('email', '=', $request['email'])->first();

        if ($user) {
            event(new ResetPassword($request['token']));
        } else
            return response()->json(['email' => 'incorrect'], 500);

    }

    public function checkUnique(Request $request)
    {
        $request->validate(['email' => 'unique:users,email', 'login' => 'unique:users,name']);
    }
}
